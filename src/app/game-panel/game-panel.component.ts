import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../service/global-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game-panel',
  templateUrl: './game-panel.component.html',
  styleUrls: ['./game-panel.component.scss']
})
export class GamePanelComponent implements OnInit {

  totalScore: number = 0;
  article: string = '';
  

  constructor(private globalDataService: GlobalDataService, private router: Router) { }

  ngOnInit() {
    //console.log(this.globalDataService.data.screen_1.game_started);
    if (this.globalDataService.data.screen_1.game_started === false) {
      this.router.navigateByUrl('screen-1');
    }
  }

  receiveFromChildTop(event) {
    console.log(event);
    if (event && event.article) {
      this.article = event.article;
      this.totalScore += 10;
      this.globalDataService.data.screen_2.totalScore = this.totalScore;
      console.log(this.totalScore);
    }
  }

  timeOver(time){
    console.log(time);
    this.router.navigateByUrl('screen-3');
  }


}
