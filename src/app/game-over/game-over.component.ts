import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../service/global-data.service';
import { Router } from '@angular/router';
import { ApiServiceService } from '../service/api-service.service';

@Component({
  selector: 'app-game-over',
  templateUrl: './game-over.component.html',
  styleUrls: ['./game-over.component.scss']
})
export class GameOverComponent implements OnInit {
  totalScore = 0;
  apiId = 'igh1u';

  getUrl = 'https://api.myjson.com/bins/'+this.apiId;
  postUrl = 'https://api.myjson.com/bins';
  putUrl = 'https://api.myjson.com/bins/'+this.apiId;

  headers = {
    'content-type': 'application/json',
    // 'secret-key': this.secretKey,
    'private': 'true'
  }

  data = { "score": 0 };

  constructor(private apiServiceService: ApiServiceService, private globalDataService: GlobalDataService, private router: Router) { }

  ngOnInit() {
    if (this.globalDataService.data.screen_1.game_started === false) {
      this.router.navigateByUrl('screen-1');
    }
    this.totalScore = this.globalDataService.data.screen_2.totalScore;
    this.data = { "score": this.totalScore };

    this.updateJSON(this.putUrl, this.data, this.headers);
  }

  updateJSON(url: string, data: any, headers?: any) {
    this.apiServiceService.putJSON(url, data, headers).subscribe((res) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }

}
