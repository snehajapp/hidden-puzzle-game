import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bottom-panel',
  templateUrl: './bottom-panel.component.html',
  styleUrls: ['./bottom-panel.component.scss']
})


export class BottomPanelComponent implements OnInit {
  @Input('totalScoreFromGP') private totalScoreFromGP:number = 0;
  @Input('articleFromGP') private articleFromGP:string = '';

  dogMoved: boolean = false;
  cakeMoved: boolean = false;
  cupcakeMoved: boolean = false;
  saltMoved: boolean = false;
  bottleMoved: boolean = false;
  fruitMoved: boolean = false;
  guitarMoved: boolean = false;
  owlMoved: boolean = false;
  birdMoved: boolean = false;
  mixerMoved: boolean = false;
  glassMoved: boolean = false;
  hatMoved: boolean = false;
  candleMoved: boolean = false;
  lizardMoved: boolean = false;
  glovesMoved: boolean = false;
  tissueMoved: boolean = false;
  diceMoved: boolean = false;
  spoonMoved: boolean = false;
  broomMoved: boolean = false;
 
  totalScore:number = 0;

  maxScore:number = 180;
  
  constructor(private router:Router) { }

  ngOnInit() {
    console.log('ngOnInit', this.totalScore, this.totalScoreFromGP);
  }

  ngOnChanges(){
    console.log('ngOnChanges', this.totalScore, this.totalScoreFromGP, this.articleFromGP);
    this.totalScore = this.totalScoreFromGP;

    if(this.totalScore >= this.maxScore){
      this.router.navigateByUrl('screen-3');
    }

    if(this.articleFromGP === 'dog'){
      this.dogMoved = true;
    }

    if(this.articleFromGP === 'cake'){
      this.cakeMoved = true;
    }

    if(this.articleFromGP === 'cupcake'){
      this.cupcakeMoved = true;
    }

    if(this.articleFromGP === 'guitar'){
      this.guitarMoved = true;
    }

    if(this.articleFromGP === 'broom'){
      this.broomMoved = true;
    }

    if(this.articleFromGP === 'salt'){
      this.saltMoved = true;
    }

    if(this.articleFromGP === 'bottle'){
      this.bottleMoved = true;
    }

    if(this.articleFromGP === 'fruit'){
      this.fruitMoved = true;
    }

    if(this.articleFromGP === 'gloves'){
      this.glovesMoved = true;
    }

    if(this.articleFromGP === 'lizard'){
      this.lizardMoved = true;
    }

    if(this.articleFromGP === 'candle'){
      this.candleMoved = true;
    }

    if(this.articleFromGP === 'mixer'){
      this.mixerMoved = true;
    }

    if(this.articleFromGP === 'glass'){
      this.glassMoved = true;
    }

    if(this.articleFromGP === 'hat'){
      this.hatMoved = true;
    }

    if(this.articleFromGP === 'dice'){
      this.diceMoved = true;
    }

    if(this.articleFromGP === 'tissue'){
      this.tissueMoved = true;
    }

    if(this.articleFromGP === 'bird'){
      this.birdMoved = true;
    }

    if(this.articleFromGP === 'owl'){
      this.owlMoved = true;
    }

    if(this.articleFromGP === 'spoon'){
      this.spoonMoved = true;
    }
  }

}
