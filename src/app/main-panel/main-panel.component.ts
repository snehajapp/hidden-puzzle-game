import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalDataService } from '../service/global-data.service';

@Component({
  selector: 'app-main-panel',
  templateUrl: './main-panel.component.html',
  styleUrls: ['./main-panel.component.scss']
})
export class MainPanelComponent implements OnInit {

  constructor(private router:Router, private globalDataService:GlobalDataService) { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    var h1 = document.querySelector("h1");

    h1.addEventListener("input", function () {
      this.setAttribute("data-text", this.innerText);
    });
  }

  navigateTo(pageName){
    //console.log(pageName);
    this.globalDataService.data.screen_1.game_started = true;
    this.router.navigateByUrl(pageName);
  }





}
