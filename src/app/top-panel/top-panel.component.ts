import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-top-panel',
  templateUrl: './top-panel.component.html',
  styleUrls: ['./top-panel.component.scss']
})
export class TopPanelComponent implements OnInit {
  //@Input('')
  @Output('updateToParent') updateToParent: EventEmitter<any> = new EventEmitter();

  showEffect: boolean = false;

  showDogQuote: boolean = false;
  showCakeQuote: boolean = false;
  showDiceQuote: boolean = false;
  showBottleQuote: boolean = false;
  showSaltQuote: boolean = false;
  showCupcakeQuote: boolean = false;
  showGuitarQuote: boolean = false;
  showBroomQuote: boolean = false;
  showOwlQuote: boolean = false;
  showFruitQuote: boolean = false;
  showSpoonQuote: boolean = false;
  showGlovesQuote: boolean = false;
  showLizardQuote: boolean = false;
  showMixerQuote: boolean = false;
  showCandleQuote: boolean = false;
  showGlassQuote: boolean = false;
  showTissueQuote: boolean = false;
  showHatQuote: boolean = false;
  showBirdQuote: boolean = false;

  effectTop: number = 0;
  effectLeft: number = 0;

  position = {
    dog: {
      top: 319,
      left: 0
    },
    cake: {
      top: 425,
      left: 187
    },
    dice: {
      top: 460,
      left: 222
    },
    bottle: {
      top: 464,
      left: 477
    },
    salt: {
      top: 460,
      left: 420
    },
    cupcake: {
      top: 250,
      left: 140
    },
    guitar: {
      top: 220,
      left: 80
    },
    broom: {
      top: 160,
      left: 220
    },
    owl: {
      top: 80,
      left: 280
    },
    fruit: {
      top: 420,
      left: 490
    },
    spoon: {
      top: 440,
      left: 600
    },
    gloves: {
      top: 400,
      left: 870
    },
    lizard: {
      top: 300,
      left: 870
    },
    mixer: {
      top: 160,
      left: 450
    },
    candle: {
      top: 30,
      left: 840
    },
    glass: {
      top: 160,
      left: 500
    },
    tissue: {
      top: 460,
      left: 350
    },
    hat: {
      top: 260,
      left: 540
    },
    bird: {
      top: 0,
      left: 410
    }

  }

  constructor() { }

  ngOnInit() {
  }

  showQuoteForDog(object) {
    this.effectTop = this.position.dog.top;
    this.effectLeft = this.position.dog.left;

    this.showEffect = true;
    console.log(object);



    if (this.showDogQuote) {
      this.showDogQuote = false;
    } else {
      this.showDogQuote = true;
    }
    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    this.updateToParent.emit({ article: 'dog' });

  }

  showQuoteForCake(object) {
    this.effectTop = this.position.cake.top;
    this.effectLeft = this.position.cake.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showCakeQuote) {
      this.showCakeQuote = false;
    } else {
      this.showCakeQuote = true;
    }

    this.updateToParent.emit({ article: 'cake' });

  }

  showQuoteForDice(object) {
    this.effectTop = this.position.dice.top;
    this.effectLeft = this.position.dice.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showDiceQuote) {
      this.showDiceQuote = false;
    } else {
      this.showDiceQuote = true;
    }

    this.updateToParent.emit({ article: 'dice' });
  }

  showQuoteForBottle(object) {
    this.effectTop = this.position.bottle.top;
    this.effectLeft = this.position.bottle.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showBottleQuote) {
      this.showBottleQuote = false;
    } else {
      this.showBottleQuote = true;
    }

    this.updateToParent.emit({ article: 'bottle' });
  }

  showQuoteForSalt(object) {
    this.effectTop = this.position.salt.top;
    this.effectLeft = this.position.salt.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showSaltQuote) {
      this.showSaltQuote = false;
    } else {
      this.showSaltQuote = true;
    }
    this.updateToParent.emit({ article: 'salt' });
  }

  showQuoteForCupcake(object) {
    this.effectTop = this.position.cupcake.top;
    this.effectLeft = this.position.cupcake.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showCupcakeQuote) {
      this.showCupcakeQuote = false;
    } else {
      this.showCupcakeQuote = true;
    }
    this.updateToParent.emit({ article: 'cupcake' });
  }

  showQuoteForGuitar(object) {
    this.effectTop = this.position.guitar.top;
    this.effectLeft = this.position.guitar.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);
    if (this.showGuitarQuote) {
      this.showGuitarQuote = false;
    } else {
      this.showGuitarQuote = true;
    }
    this.updateToParent.emit({ article: 'guitar' });
  }

  showQuoteForBroom(object) {
    this.effectTop = this.position.broom.top;
    this.effectLeft = this.position.broom.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);
    if (this.showBroomQuote) {
      this.showBroomQuote = false;
    } else {
      this.showBroomQuote = true;
    }
    this.updateToParent.emit({ article: 'broom' });
  }

  showQuoteForOwl(object) {
    this.effectTop = this.position.owl.top;
    this.effectLeft = this.position.owl.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);
    if (this.showOwlQuote) {
      this.showOwlQuote = false;
    } else {
      this.showOwlQuote = true;
    }
    this.updateToParent.emit({ article: 'owl' });
  }

  showQuoteForFruit(object) {
    this.effectTop = this.position.fruit.top;
    this.effectLeft = this.position.fruit.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);
    if (this.showFruitQuote) {
      this.showFruitQuote = false;
    } else {
      this.showFruitQuote = true;
    }
    this.updateToParent.emit({ article: 'fruit' });
  }

  showQuoteForSpoon(object) {
    this.effectTop = this.position.spoon.top;
    this.effectLeft = this.position.spoon.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showSpoonQuote) {
      this.showSpoonQuote = false;
    } else {
      this.showSpoonQuote = true;
    }
    this.updateToParent.emit({ article: 'spoon' });
  }

  showQuoteForGloves(object) {
    this.effectTop = this.position.gloves.top;
    this.effectLeft = this.position.gloves.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showGlovesQuote) {
      this.showGlovesQuote = false;
    } else {
      this.showGlovesQuote = true;
    }
    this.updateToParent.emit({ article: 'gloves' });
  }

  showQuoteForLizard(object) {
    this.effectTop = this.position.lizard.top;
    this.effectLeft = this.position.lizard.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showLizardQuote) {
      this.showLizardQuote = false;
    } else {
      this.showLizardQuote = true;
    }
    this.updateToParent.emit({ article: 'lizard' });
  }

  showQuoteForMixer(object) {
    this.effectTop = this.position.mixer.top;
    this.effectLeft = this.position.mixer.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showMixerQuote) {
      this.showMixerQuote = false;
    } else {
      this.showMixerQuote = true;
    }
    this.updateToParent.emit({ article: 'mixer' });
  }

  showQuoteForCandle(object) {
    this.effectTop = this.position.candle.top;
    this.effectLeft = this.position.candle.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showCandleQuote) {
      this.showCandleQuote = false;
    } else {
      this.showCandleQuote = true;
    }
    this.updateToParent.emit({ article: 'candle' });
  }

  showQuoteForGlass(object) {
    this.effectTop = this.position.glass.top;
    this.effectLeft = this.position.glass.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showGlassQuote) {
      this.showGlassQuote = false;
    } else {
      this.showGlassQuote = true;
    }
    this.updateToParent.emit({ article: 'glass' });
  }

  showQuoteForTissue(object) {
    this.effectTop = this.position.tissue.top;
    this.effectLeft = this.position.tissue.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showTissueQuote) {
      this.showTissueQuote = false;
    } else {
      this.showTissueQuote = true;
    }
    this.updateToParent.emit({ article: 'tissue' });
  }

  showQuoteForHat(object) {
    this.effectTop = this.position.hat.top;
    this.effectLeft = this.position.hat.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showHatQuote) {
      this.showHatQuote = false;
    } else {
      this.showHatQuote = true;
    }
    this.updateToParent.emit({ article: 'hat' });
  }

  showQuoteForBird(object) {
    this.effectTop = this.position.bird.top;
    this.effectLeft = this.position.bird.left;

    this.showEffect = true;

    setTimeout(() => {
      this.showEffect = false;
    }, 2000);

    if (this.showBirdQuote) {
      this.showBirdQuote = false;
    } else {
      this.showBirdQuote = true;
    }
    this.updateToParent.emit({ article: 'bird' });
  }

}
