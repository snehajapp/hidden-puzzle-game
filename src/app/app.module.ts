import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainPanelComponent } from './main-panel/main-panel.component';
import { BottomPanelComponent } from './bottom-panel/bottom-panel.component';
import { GameConsoleComponent } from './game-console/game-console.component';
import { TopPanelComponent } from './top-panel/top-panel.component';
import { GameOverComponent } from './game-over/game-over.component';
import { Routes, RouterModule } from '@angular/router';
import { GamePanelComponent } from './game-panel/game-panel.component';
import { TimerComponent } from './timer/timer.component';
import { HttpClientModule } from '@angular/common/http';

const appRoutes: Routes = [
  { path: '', redirectTo: 'screen-1', pathMatch: 'full' },
  { path: 'screen-1', component: MainPanelComponent },
  { path: 'screen-2', component: GamePanelComponent },
  { path: 'screen-3', component: GameOverComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MainPanelComponent,
    BottomPanelComponent,
    GameConsoleComponent,
    TopPanelComponent,
    GameOverComponent,
    GamePanelComponent,
    TimerComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
